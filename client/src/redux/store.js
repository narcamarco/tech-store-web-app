import { combineReducers, configureStore } from '@reduxjs/toolkit';
import cart from './slices/cart';
import order from './slices/order';
import products from './slices/products';
import user from './slices/user';
import admin from './slices/admin';

const reducer = combineReducers({
  products,
  cart,
  user,
  order,
  admin,
});

export default configureStore({
  reducer,
});
