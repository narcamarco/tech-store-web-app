import { createSlice } from '@reduxjs/toolkit';

const calculateSubTotal = (cartState) => {
  let result = 0;
  cartState.map((item) => {
    return (result += item.qty * item.price);
  });

  return Number(result).toFixed(2);
};

const updateLocalStorage = (cart) => {
  localStorage.setItem('cartItems', JSON.stringify(cart));
  localStorage.setItem('subtotal', JSON.stringify(calculateSubTotal(cart)));
};

export const initialState = {
  loading: false,
  error: null,
  cart: JSON.parse(localStorage.getItem('cartItems')) ?? [],
  expressShipping: JSON.parse(localStorage.getItem('expressShipping')) ?? false,
  subTotal: localStorage.getItem('cartItems')
    ? calculateSubTotal(JSON.parse(localStorage.getItem('cartItems')))
    : 0,
};

export const cartSlice = createSlice({
  name: 'cart',
  initialState,
  reducers: {
    setLoading: (state) => {
      state.loading = true;
    },
    cartItemAdd: (state, { payload }) => {
      const existingItem = state.cart.find((item) => {
        return item.id === payload.id;
      });

      if (existingItem) {
        state.cart = state.cart.map((item) => {
          return item.id === existingItem.id ? payload : item;
        });
      } else {
        state.cart = [...state.cart, payload];
      }

      state.loading = false;
      state.error = null;
      updateLocalStorage(state.cart);
      state.subTotal = calculateSubTotal(state.cart);
    },
    cartItemRemoval: (state, { payload }) => {
      state.cart = [...state.cart].filter((item) => item.id !== payload);
      updateLocalStorage(state.cart);
      state.subTotal = calculateSubTotal(state.cart);
      state.loading = false;
      state.error = null;
    },
    setExpressShipping: (state, { payload }) => {
      state.expressShipping = payload;
      localStorage.setItem('expressShipping', payload);
    },
    clearCart: (state) => {
      localStorage.removeItem('cartItems');
      state.cart = [];
    },
    setError: (state, { payload }) => {
      state.error = payload;
      state.loading = false;
    },
  },
});

export const {
  setLoading,
  cartItemAdd,
  setError,
  cartItemRemoval,
  setExpressShipping,
  clearCart,
} = cartSlice.actions;

export default cartSlice.reducer;

export const cartSelector = (state) => state.cart;
