import React, { useEffect, useState } from 'react';
import {
  Box,
  Button,
  FormControl,
  Heading,
  HStack,
  Stack,
  Text,
  AlertDescription,
  AlertIcon,
  AlertTitle,
  Flex,
  Card,
  CardHeader,
  CardBody,
  StackDivider,
  useToast,
  Alert,
} from '@chakra-ui/react';
import { useDispatch, useSelector } from 'react-redux';
import { Navigate, useLocation } from 'react-router-dom';
import TextField from '../components/TextField';
import PasswordTextField from '../components/PasswordTextField';
import { Formik } from 'formik';
import * as Yup from 'yup';
import {
  resetUpdateSuccess,
  updateProfile,
} from '../redux/actions/userActions';

const ProfileScreen = () => {
  const dispatch = useDispatch();
  const user = useSelector((state) => state.user);
  const { userInfo, error, loading, updateSuccess } = user;
  const location = useLocation();
  const toast = useToast();

  useEffect(() => {
    if (updateSuccess) {
      toast({
        description: 'Profile Saved',
        status: 'success',
        isClosable: true,
      });
      dispatch(resetUpdateSuccess());
    }
  }, [updateSuccess, toast, dispatch]);

  return userInfo ? (
    <Formik
      initialValues={{
        email: userInfo.email,
        password: '',
        name: userInfo.name,
        confirmPassword: '',
      }}
      validationSchema={Yup.object({
        name: Yup.string().required('Full Name is required'),
        email: Yup.string()
          .email('Invalid Email.')
          .required('An Email Address is required'),
        password: Yup.string().min(
          1,
          'Password is too short - must contain at least 1 character.'
        ),
        confirmPassword: Yup.string()
          .min(1, 'Password is too short - must contain at least 1 character.')
          .oneOf([Yup.ref('password'), null], 'Password must match.'),
      })}
      onSubmit={(values) => {
        dispatch(
          updateProfile(
            userInfo._id,
            values.name,
            values.email,
            values.password
          )
        );
      }}
    >
      {(formik) => {
        return (
          <Box
            minH="100vh"
            maxW={{ base: '3xl', lg: '7xl' }}
            mx="auto"
            px={{ base: '4', md: '8', lg: '12' }}
            py={{ base: '6', md: '8', lg: '12' }}
          >
            <Stack
              spacing="10"
              direction={{ base: 'column', lg: 'row' }}
              align={{ lg: 'flex-start' }}
            >
              <Stack flex="1.5" md={{ base: '2xl', md: 'none' }}>
                <Heading fontSize="2xl" fontWeight="extrabold">
                  Profile
                </Heading>

                <Stack spacing="6">
                  <Stack spacing="6" as="form" onSubmit={formik.handleSubmit}>
                    {error && (
                      <Alert
                        status="error"
                        flexDirection="column"
                        alignItems="center"
                        justifyContent="center"
                        textAlign="center"
                      >
                        <AlertIcon />
                        <AlertTitle>We are sorry!</AlertTitle>
                        <AlertDescription>{error}</AlertDescription>
                      </Alert>
                    )}

                    <Stack spacing="5">
                      <FormControl>
                        <TextField
                          type="text"
                          name="name"
                          placeholder="You first and last name"
                          label="Full Name"
                        />
                        <TextField
                          type="text"
                          name="email"
                          placeholder="example@gmail.com"
                          label="email"
                        />
                        <PasswordTextField
                          type="password"
                          name="password"
                          placeholder="your password"
                          label="Password"
                        />
                        <PasswordTextField
                          type="password"
                          name="confirmPassword"
                          placeholder="Confirm your Password"
                          label="Confirm Password"
                        />
                      </FormControl>
                    </Stack>

                    <Stack spacing="6">
                      <Button
                        colorScheme="orange"
                        size="lg"
                        fontSize="md"
                        isLoading={loading}
                        type="submit"
                      >
                        Save
                      </Button>
                    </Stack>
                  </Stack>
                </Stack>
              </Stack>
              <Flex
                direction="column"
                align="center"
                flex="1"
                _dark={{ bg: 'gray.900' }}
              >
                <Card>
                  <CardHeader>
                    <Heading size="md">User Report</Heading>
                  </CardHeader>

                  <CardBody>
                    <Stack divider={<StackDivider />} spacing="4">
                      <Box pt="2" fontSize="sm">
                        Registered on{' '}
                        {new Date(userInfo.createdAt).toDateString()}
                      </Box>
                    </Stack>
                  </CardBody>
                </Card>
              </Flex>
            </Stack>
          </Box>
        );
      }}
    </Formik>
  ) : (
    <Navigate to="/login" replace={true} state={{ from: location }} />
  );
};

export default ProfileScreen;
