import React, { useEffect, useState, useRef } from 'react';
import {
  Box,
  TableContainer,
  Th,
  Tr,
  Table,
  Td,
  Thead,
  Tbody,
  Button,
  useDisclosure,
  Alert,
  AlertIcon,
  Stack,
  Spinner,
  AlertTitle,
  AlertDescription,
  Wrap,
  useToast,
} from '@chakra-ui/react';
import { useDispatch, useSelector } from 'react-redux';
import { CheckCircleIcon, DeleteIcon } from '@chakra-ui/icons';
import {
  getAllUsers,
  deleteUser,
  resetErrorAndRemoval,
} from '../redux/actions/adminActions';
import ConfirmRemovalAlert from './ConfirmRemovalAlert';

const UsersTab = () => {
  const { isOpen, onOpen, onClose } = useDisclosure();
  const cancelRef = useRef();
  const [userToDelete, setUserToDelete] = useState('');
  const dispatch = useDispatch();
  const toast = useToast();

  const admin = useSelector((state) => state.admin);
  const user = useSelector((state) => state.user);

  const { error, loading, userRemoval, userList } = admin;
  const { userInfo } = user;

  useEffect(() => {
    dispatch(getAllUsers());
    dispatch(resetErrorAndRemoval());

    if (userRemoval) {
      toast({
        description: 'User has been removed',
        status: 'success',
        isClosable: true,
      });
    }
  }, [dispatch, userRemoval, toast]);

  const openDeleteConfirmBox = (user) => {
    setUserToDelete(user);
    onOpen();
  };

  return (
    <Box>
      {error && (
        <Alert status="error">
          <AlertIcon />
          <AlertTitle>Opps!</AlertTitle>
          <AlertDescription>{error}</AlertDescription>
        </Alert>
      )}

      {loading ? (
        <Wrap justify="center">
          <Stack direction="row" spacing="4">
            <Spinner
              mt="20"
              thickness="2px"
              speed="0.65s"
              emptyColor="gray.200"
              color="orange.500"
              size="xl"
            />
          </Stack>
        </Wrap>
      ) : (
        <Box>
          <TableContainer>
            <Table variant="simple">
              <Thead>
                <Tr>
                  <Th>Name</Th>
                  <Th>Email</Th>
                  <Th>Registered</Th>
                  <Th>Admin</Th>
                  <Th>Action</Th>
                </Tr>
              </Thead>

              <Tbody>
                {userList &&
                  userList.map((user) => {
                    return (
                      <Tr key={user._id}>
                        <Td>
                          {user.name}
                          {user._id === userInfo._id ? '(You)' : null}
                        </Td>
                        <Td>{user.email}</Td>
                        <Td>{new Date(user.createdAt).toDateString()}</Td>
                        <Td>
                          {user.isAdmin === 'true' ? (
                            <CheckCircleIcon color="orange.500" />
                          ) : null}
                        </Td>
                        <Td>
                          <Button
                            isDisabled={user._id === userInfo._id}
                            variant="outline"
                            onClick={() => openDeleteConfirmBox(user)}
                          >
                            <DeleteIcon mr="5px" /> Remove User
                          </Button>
                        </Td>
                      </Tr>
                    );
                  })}
              </Tbody>
            </Table>
          </TableContainer>
          <ConfirmRemovalAlert
            isOpen={isOpen}
            onOpen={onOpen}
            onClose={onClose}
            cancelRef={cancelRef}
            itemToDelete={userToDelete}
            deleteAction={deleteUser}
          />
        </Box>
      )}
    </Box>
  );
};

export default UsersTab;
