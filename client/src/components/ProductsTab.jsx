import React, { useEffect, useRef } from 'react';
import {
  Box,
  TableContainer,
  Th,
  Tr,
  Table,
  Thead,
  Tbody,
  useDisclosure,
  Alert,
  AlertIcon,
  Stack,
  Spinner,
  AlertTitle,
  AlertDescription,
  Wrap,
  Text,
  useToast,
  Accordion,
  AccordionItem,
  AccordionButton,
  AccordionPanel,
} from '@chakra-ui/react';
import { useDispatch, useSelector } from 'react-redux';
import {
  getProducts,
  resetProductError,
} from '../redux/actions/productActions';
import ProductTableItem from './ProductTableItem';
import AddNewProduct from './AddNewProduct';

const ProductsTab = () => {
  const dispatch = useDispatch();
  const toast = useToast();

  const admin = useSelector((state) => state.admin);
  const { error, loading } = admin;

  const productInfo = useSelector((state) => state.products);
  const { products, productUpdate } = productInfo;

  useEffect(() => {
    dispatch(getProducts);
    dispatch(resetProductError());

    if (productUpdate) {
      toast({
        description: 'Product has been updated',
        status: 'success',
        isClosable: true,
      });
    }
  }, [dispatch, productUpdate, toast]);

  return (
    <Box>
      {error && (
        <Alert status="error">
          <AlertIcon />
          <AlertTitle>Opps!</AlertTitle>
          <AlertDescription>{error}</AlertDescription>
        </Alert>
      )}

      {loading ? (
        <Wrap justify="center">
          <Stack direction="row" spacing="4">
            <Spinner
              mt="20"
              thickness="2px"
              speed="0.65s"
              emptyColor="gray.200"
              color="orange.500"
              size="xl"
            />
          </Stack>
        </Wrap>
      ) : (
        <Box>
          <Accordion allowToggle>
            <AccordionItem>
              <h2>
                <AccordionButton>
                  <Box flex="1" textAlign="right">
                    <Box>
                      <Text mr="8px" fontWeight="bold">
                        Add New Product
                      </Text>
                    </Box>
                  </Box>
                </AccordionButton>
              </h2>

              <AccordionPanel pb="4">
                <Table>
                  <Tbody>
                    <AddNewProduct />
                  </Tbody>
                </Table>
              </AccordionPanel>
            </AccordionItem>
          </Accordion>

          <Table variant="simple" size="lg">
            <Thead>
              <Tr>
                <Th>Image</Th>
                <Th>Description</Th>
                <Th>Brand & Name</Th>
                <Th>Category & Price</Th>
                <Th>Stock & New Badge</Th>
              </Tr>
            </Thead>

            <Tbody>
              {products.length > 0 &&
                products.map((product) => {
                  return (
                    <ProductTableItem product={product} key={product._id} />
                  );
                })}
            </Tbody>
          </Table>
        </Box>
      )}
    </Box>
  );
};

export default ProductsTab;
